import React from 'react'
import { Link } from 'react-router-dom'
import {Divider} from '@material-ui/core';

export default function header() {
    return (
        <div style={headerStyle}>
                <header>
                    <img src={process.env.PUBLIC_URL + './WNElogo.png'} style={{ maxWidth:'450px', height: '60', float: 'left'}}></img>
                    <Divider orientation="vertical" flexItem />
                    <h2 style={{color: 'white', marginLeft: '450px'}}>Bear Necessities Market</h2>
                </header>
        </div>
    )
}

const headerStyle = {
    background: '#004B8D',
    color: 'white',
    zindex: 5000,
    margin: 'auto',
    padding: '17px',
    whiteSpace: 'nowrap',
    align: 'center',
    display: 'flex',
    
}