import React from 'react'
import { Link } from 'react-router-dom'
import {Button, AppBar, Typography, Toolbar} from '@material-ui/core';

import { makeStyles, withStyles } from '@material-ui/core/styles';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

export default function OrderListAppBar() {
    const classes = useStyles();
    return (
        <div className={classes.root}>
        <AppBar className={classes.appbar} position="static">
            <Toolbar variant="dense" disableGutters="true">

          <BackButton><ArrowBackIosIcon style={{ color: 'green'}}/>Back to Menu</BackButton>
           
            <Typography variant="h4" align="center" className={classes.title}>Orders</Typography>
            </Toolbar>
        </AppBar>
        </div>
    )
}

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      marginLeft: '20%',
      marginRight: '20%',
      minWidth: '735px'
    },
    appbar: {
        display: 'flex',
        flexDirection: 'column',
        background: 'rgb(253, 184, 19)'
    },
    icon: {
        color: 'black'
    },
    title: {
    flexGrow: 1,
    color: 'black',
    marginRight: '210px'
    }
  }));


const BackButton = withStyles({
    root: {
        padding: '13px 8px',
        color: 'inherit'
    },
    label: {
        color: 'black',
        fontWeight: 'bold'
    },
})(Button);
