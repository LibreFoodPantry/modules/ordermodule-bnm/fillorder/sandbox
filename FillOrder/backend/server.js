const app = require('./app');
const mongoose = require("mongoose");
require("dotenv").config();

var port = process.env.PORT || 3001;

mongoose.Promise = global.Promise;
// mongoose.connect(process.env.FO_MONGODB);

mongoose.set('useUnifiedTopology', true);
mongoose.connect(process.env.FO_MONGODB, { useNewUrlParser: true });

app.listen(port);
console.log(`FillOrder API Service running on port: ${port}`);