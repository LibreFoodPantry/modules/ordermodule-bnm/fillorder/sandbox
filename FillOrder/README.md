## Running the Project

### Pre-Requisites:
* Install the most recent version of [Docker Toolbox](https://github.com/docker/toolbox/releases) (should include VirtualBox)
* **Clone** the [FullStack](https://gitlab.com/LibreFoodPantry/modules/ordermodule-bnm/fillorder/fullstack) project from GitLab
### Instructions:
* After cloning the FullStack project, open the new '**fullstack**' directory that's created
* Then open the '**FillOrder**' folder inside the directory
* This is the directory that **_docker-compose up_** needs to be run from, so open a command prompt window at this directory
* First, run **_docker-machine ls_** to see if any docker machines are currently running. If not, run **_docker-machine start_**, and the 'default' machine should start up.
* Once the machine has started, run **_docker-compose up --build_**. 
    * **Note**: This will take a while to run because npm needs to install dependencies inside of the docker container. The command prompt will look like the matrix for a little while; don't worry, it's supposed to do that.
    * **Note**: To run in a production environment instead, run **_docker-compose -f docker-compose-prod.yml up --build_**
* Eventually, the console will halt its output and the project should be running.
* Now navigate to [http://192.168.99.100](http://192.168.99.100)
    * **Note** : If this address does not work, check which IP address docker is using by running **_docker-machine ip_** in another command prompt, and replace "192.168.99.100" with the address that is displayed
    * **If you are not using Docker Toolbox** the address is most likely either **0.0.0.0** or **127.0.0.1**

### Running Tests:
* To run tests, a separate console has to be opened
* **Backend**:
  * Run the command **_docker container exec -it fill_order_api npm test_**
  * This command accesses the docker container running the backend (*fill_order_api*), and runs the command **npm test**

### URLs:
* The base URL of **_192.168.99.100_** is really just a page that mocks how the finalized navigation screen might work for BNM. It consists of an iFrame pointing to the frontend service.
* To access the **frontend service** explicitly, navigate to **_192.168.99.100/fill-order_**
* The **backend service** can be reached through **_192.168.99.100/fill-order/api_**
